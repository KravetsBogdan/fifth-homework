// 1) Метод в обʼєкті це властивості із значеннями в якких є функції. Методи використовуються для роботи із 
// властивостями обʼєкта в середині обʼєкта при цьому не засмічуючи глобальну область.
// 2) Обʼєкт може мати будь який тип даних;


let newUser = {};
function createNewUser(firstName, lastName) {
    while (!firstName || !isNaN(firstName)) {
        firstName = prompt('Введіть ваше ім\'я')?.trim();
    }
    while (!lastName || !isNaN(lastName)) {
        lastName = prompt('Введіть вашу фамілію')?.trim();
    }
    newUser = {
        firstName,
        lastName,
        getLogin() {
            let test = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            return test;
        },
    };
    return newUser;
};
createNewUser();
console.log(newUser);
console.log(newUser.getLogin());